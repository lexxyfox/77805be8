extends CharacterGridMovement


func move(dir: Vector2):
	character.face(dir)
	if character.get_node('Tween').is_active():
		return
	var angle = dir.angle()
	# I'll do gud math later, promise!
	match int((angle / TAU) * 8):
		-3:
			character.ray2.position = Vector2(0, -64)
			character.ray2.cast_to = Vector2(-32, 0)
			character.ray.cast_to = Vector2(0, -64)
		-2:
			character.ray2.position = Vector2(0, -64)
			character.ray2.cast_to = Vector2(32, 0)
			character.ray.cast_to = Vector2(0, -64)
		-1:
			character.ray2.position = Vector2(64, 0)
			character.ray2.cast_to = Vector2(0, -32)
			character.ray.cast_to = Vector2(64, 0)
		0:
			character.ray2.position = Vector2(64, 0)
			character.ray2.cast_to = Vector2(0, 32)
			character.ray.cast_to = Vector2(64, 0)
		1:
			character.ray2.position = Vector2(0, 64)
			character.ray2.cast_to = Vector2(32, 0)
			character.ray.cast_to = Vector2(0, 64)
		2:
			character.ray2.position = Vector2(0, 64)
			character.ray2.cast_to = Vector2(-32, 0)
			character.ray.cast_to = Vector2(0, 64)
		3:
			character.ray2.position = Vector2(-64, 0)
			character.ray2.cast_to = Vector2(0, 32)
			character.ray.cast_to = Vector2(-64, 0)
		4:
			character.ray2.position = Vector2(-64, 0)
			character.ray2.cast_to = Vector2(0, -32)
			character.ray.cast_to = Vector2(-64, 0)
	character.ray.force_raycast_update()
	character.ray2.force_raycast_update()
	if character.ray.is_colliding() || character.ray2.is_colliding():
		return #bump
	var target = character.position + character.ray.cast_to
	character.get_node('Tween').interpolate_property(
		character, 'position', character.position, target,
		(character.ray.cast_to.length() / character.world.cell_size.x) / character.speed,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	character.get_node('Tween').stop(self, 'position')
	character.get_node('Tween').start()
	yield(character.get_node('Tween'), "tween_completed")
	target = character.position + character.ray2.cast_to
	character.get_node('Tween').interpolate_property(
		character, 'position', character.position, target,
		(character.ray2.cast_to.length() / character.world.cell_size.x) / character.speed,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)

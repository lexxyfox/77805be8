class_name CharacterGridMovement extends CharacterState

func enter(_msg = {}):
	character.position = character.world.map_to_world(character.world.world_to_map(character.position))
	character.face(character.facing.cast_to)

func move(dir: Vector2):
	character.face(dir)
	if character.get_node('Tween').is_active():
		return
	character.ray.cast_to = dir * character.world.cell_size
	character.ray.force_raycast_update()
	if character.ray.is_colliding():
		return #bump
	var target = character.position + character.ray.cast_to
	character.get_node('Tween').interpolate_property(
		character, 'position', character.position, target,
		(character.position.distance_to(target) / character.world.cell_size.x) / character.speed,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	character.get_node('Tween').stop(self, 'position')
	character.get_node('Tween').start()
	yield(character.get_node('Tween'), "tween_completed")

extends KinematicBody2D

enum open_states {
	OPEN,
	CLOSED,
}

export(open_states) var open_state = open_states.CLOSED

func interacted_with(interactor):
	print('Door was interacted with by', interactor)
	match open_state:
		open_states.CLOSED:
			$AnimationPlayer.play('Normal')
			set_collision_layer_bit(0, false)
			set_collision_mask_bit(0, false)
			$LightOccluder2D.set_occluder_light_mask(0)
			yield($AnimationPlayer, 'animation_finished')
			open_state = open_states.OPEN
		open_states.OPEN:
			$AnimationPlayer.play_backwards('Normal')
			set_collision_layer_bit(0, true)
			set_collision_mask_bit(0, true)
			yield($AnimationPlayer, 'animation_finished')
			$LightOccluder2D.set_occluder_light_mask(1)
			open_state = open_states.CLOSED
			

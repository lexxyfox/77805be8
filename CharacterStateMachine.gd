class_name CharacterStateMachine extends Node

# Path to the initial active state. We export it to be able to pick the initial state in the inspector.
export var initial_state := NodePath()

# The current active state. At the start of the game, we get the `initial_state`.
onready var state: CharacterState = get_node(initial_state)

func _ready() -> void:
	yield(owner, "ready")
	# The state machine assigns itself to the State objects' state_machine property.
	for child in get_children():
		child.state_machine = self
	state.enter()

func interact() -> void:
	state.interact()

func move(dir: Vector2) -> void:
	state.move(dir)

# This function calls the current state's exit() function, then changes the active state,
# and calls its enter function.
# It optionally takes a `msg` dictionary to pass to the next state's enter() function.
func transition_to(target, msg: Dictionary = {}) -> void:
	state.exit()
	state = target
	owner.get_node('CanvasLayer/MarginContainer/VBoxContainer/HBoxContainer/MoveModeLabel').text = state.get_name()
	state.enter(msg)

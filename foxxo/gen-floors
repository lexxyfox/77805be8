#!/usr/bin/env -S deno run --no-check --allow-read --allow-write --allow-net --allow-run

import { Image } from 'https://deno.land/x/imagescript/ImageScript.js'
import * as util from './util.js'

/* floor_1.png
const file_1 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/floors_vr.dmi`)

const tiles = [
  util.get_tile(file_1, 19),
  util.get_tile(file_1, 13),
  util.get_tile(file_1, 18),
  util.get_tile(file_1, 15),
  util.get_tile(file_1,  3),
  util.get_tile(file_1, 14),
  util.get_tile(file_1, 17),
  util.get_tile(file_1, 12),
  util.get_tile(file_1, 16),
]

await util.export_png(util.layout_tiles_1(tiles))
*/

/* 
const file_1 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/floors.dmi`)

const tiles = [
  0, 1, 2, 4, 5, 14, 15,
  16, 17, 18, 19, 21, 22, 23, 27, 28, 29, 30, 31,
  40, 41, 42, 43, 44, 45, 46, 47,
  48, 49, 50, 51, 52, 53, 54, 55, 56
]

for (let i = 0; i < tiles.length; ++i) {
  tiles[i] = util.get_tile(file_1, tiles[i])
}

tiles.sort((a, b) => {
  const x = util.color_to_hsv(a.dominantColor(1, 1, 24))[0],
        y = util.color_to_hsv(b.dominantColor(1, 1, 24))[0]
  if (x != y)
    return x - y
  const v = util.color_to_hsl(a.averageColor())[2],
        w = util.color_to_hsl(b.averageColor())[2]
  return v - w
})

await util.export_png(util.layout_tiles_1(tiles))
*/

const tiles = []

const file_1 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/tiles_vr.dmi`)
for (const n of [
  0, 1, 2, 3, 4, 5, 6, 7, 8,
  9, 10, 11, 12,
  19, 20, 21, 22, 23, 24,
  32, 51, 52, 53, 54
]) {
  tiles.push(util.get_tile(file_1, n))
}

const file_2 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/tiles_old.dmi`)
for (const n of [
  0, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
]) {
  tiles.push(util.get_tile(file_2, n))
}

const file_3 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/tiles.dmi`)
for (const n of [
  0, 1, 2, 3, 4, 5,
  6, 7, 8, 14, 15, 16, 
  23, 24, 25, 26, 27
]) {
  tiles.push(util.get_tile(file_3, n))
}

const file_4 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/misc.dmi`)
for (const n of [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  10, 11, 12, 13, 14, 15, 16, 17
]) {
  tiles.push(util.get_tile(file_4, n))
}

const file_5 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/plating_vr.dmi`)
for (const n of [
  0, 1, 2, 3, 4, 5, 6, 7
]) {
  tiles.push(util.get_tile(file_5, n))
}

const file_6 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/circuit.dmi`)
for (const n of [
  0, 1, 2, 3,
]) {
  tiles.push(util.get_tile(file_6, n))
}

const file_7 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/flooring/asteroid.dmi`)
for (const n of [
  0, 5, 9, 23
]) {
  tiles.push(util.get_tile(file_7, n))
}

const file_8 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/floors_vr.dmi`)
for (const n of [
  1, 2, 3, 4, 5, 6, 7,
  8, 9, 10, 11, 12, 13, 14, 15,
  16, 17, 18, 19, 20, 21, 23, 23,
  32, 33, 34, 35, 36, 37, 38, 39,
  40, 41, 42, 43, 44, 45, 46, 47,
  48, 49
]) {
  tiles.push(util.get_tile(file_8, n))
}

const file_9 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/floors.dmi`)
for (const n of [
   0,  1,  2,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
  16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
  40, 41, 42, 43, 44, 45, 46, 47,
  48, 49, 50, 51, 52, 53, 54, 55, 56,
]) {
  tiles.push(util.get_tile(file_9, n))
}

const file_10 = await util.load_img_from_path(`${util.cs13_dir}/icons/turf/gym_ch.dmi`)
for (const n of [0, 1]) {
  tiles.push(util.get_tile(file_10, n))
}

const unique_tiles = new util.UniqueImageSet()
for (const n of tiles)
  unique_tiles.add(n)
  
const sorted_tiles = Array.from(unique_tiles)
sorted_tiles.sort((a, b) => {
  const val_thres = 2 / 3
  const x = util.color_to_hsv(a.averageColor()),
        y = util.color_to_hsv(b.averageColor())
  
  // a is colorful, b is not
  if (x[2] >= val_thres && y[2] < val_thres)
    return 1
  // b is colorful, a is not
  if (y[2] >= val_thres && x[2] < val_thres)
    return -1
  // both are colorful
  if (x[2] >= val_thres && y[2] >= val_thres)
    return x[0] - y[0]
  const v = util.color_to_hsl(a.averageColor()),
        w = util.color_to_hsl(b.averageColor())
  return v[2] - w[2]
})

await util.export_png(util.layout_tiles_1(sorted_tiles))


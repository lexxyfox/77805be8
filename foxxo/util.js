
import { Image } from 'https://deno.land/x/imagescript/ImageScript.js'
import { copy, writeAll } from 'https://deno.land/std/streams/conversion.ts'
import * as path from 'https://deno.land/std@0.125.0/path/mod.ts'



export const root_dir = path.resolve(path.dirname(new URL('.', import.meta.url).pathname))
export const cs13_dir = path.join(root_dir, '3rdparty/cs13')

export const get_tile = (image, tile, size = 32) =>
  image.clone().crop((tile * size) % image.width, Math.floor(tile / (image.width / size)) * size, size, size)

const pngcrush_cmd = 'pngcrush -v -blacken -brute -reduce -rem alla'.split(' ')

export const export_png = async (image, args = [], out = Deno.stdout, fs = Deno) => {
  const src_path = await fs.makeTempFile()
  const src_file = await fs.open(src_path, {write: true})
  const out_path = await fs.makeTempFile()
  await writeAll(src_file, await image.encode())
  await src_file.close()

  const proc = fs.run({
    cmd: [...pngcrush_cmd, ...args, src_path, out_path],
    stderr: 'null',
  })
  const { code } = await proc.status()

  const out_file = await fs.open(out_path, {read: true})
  await copy(out_file, out)
  await out_file.close()

  await fs.remove(src_path)
  await fs.remove(out_path)
  await proc.close()
  
  return code
}

export const layout_tiles = (tiles, cols) => {
  const size = tiles[0].width
  let atlas = new Image(cols * size, Math.ceil(tiles.length / cols) * size)

  for (let i = 0; i < tiles.length; ++i)
    if (tiles[i] !== null)
      atlas = atlas.composite(tiles[i], (i % cols) * size, Math.floor(i / cols) * size)
  
  
  return atlas
}

export const layout_tiles_sq = tiles => {
  return layout_tiles(tiles, Math.ceil(Math.sqrt(tiles.length)))
}

export const load_img_from_path = async path => Image.decode(await Deno.readFile(path))

export const color_to_hsl = color => {
  const rgb = Image.colorToRGBA(color)
  const r = rgb[0] / 255,
        g = rgb[1] / 255,
        b = rgb[2] / 255
  const max = Math.max(r, g, b),
        min = Math.min(r, g, b)
  const l = (max + min) / 2
  let h, s

  if (max == min)
    h = s = 0; // achromatic
  else {
    const d = max - min
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min)

    switch (max) {
      case r: h = (g - b) / d + (g < b ? 6 : 0); break
      case g: h = (b - r) / d + 2; break
      case b: h = (r - g) / d + 4; break
    }

    h /= 6
  }

  return [ h, s, l, rgb[3] / 255 ]
}

export const color_to_hsv = color => {
  const rgb = Image.colorToRGBA(color)
  const r = rgb[0] / 255,
        g = rgb[1] / 255,
        b = rgb[2] / 255
  const max = Math.max(r, g, b),
        min = Math.min(r, g, b)
  const d = max - min
  const s = max == 0 ? 0 : d / max
  let h

  if (max == min)
    h = 0; // achromatic
  else {
    switch (max) {
      case r: h = (g - b) / d + (g < b ? 6 : 0); break
      case g: h = (b - r) / d + 2; break
      case b: h = (r - g) / d + 4; break
    }

    h /= 6
  }

  return [ h, s, max, rgb[3] / 255 ]
}

export class UniqueImageSet extends Set {

  add (o) {
    for (let i of this)
      if (this.compare(o, i))
        return this
    super.add.call(this, o)
    return this
  }

  // returns false if different, true iff the same
  compare(o, i) {
    const l = o.bitmap.length
    if (l != i.bitmap.length)
      return false
    for (let x = 0; x < l; ++x)
      if (o.bitmap[x] != i.bitmap[x])
        return false
    return true
  }
}


extends CharacterState

func enter(_msg = {}):
	character.face(character.facing.cast_to)

func move(dir: Vector2):
	character.face(dir)
	if (dir.length() > 1.0):
		dir = dir.normalized()
	# warning-ignore:return_value_discarded
	character.move_and_slide(character.speed * dir * 30)


class_name Character extends KinematicBody2D

onready var world = get_parent()
onready var ray = $Ray
onready var facing = $Facing
onready var ray2 = get_node('♞Ray')

export(float) var speed = 4
export(float) var is_sitting = false
export(float) var is_smooth_movement = false

func _ready():
	if is_sitting:
		$AnimationPlayer.play('SitRight')
	else:
		$AnimationPlayer.play('Down')

func toggle_sit():
	if $StateMachine.state == $StateMachine/Sits:
		$StateMachine.transition_to($StateMachine/Sits.prev_state)
	else:
		$StateMachine.transition_to($StateMachine/Sits, {"PrevState": $StateMachine.state})

func toggle_smooth_movement():
	if is_smooth_movement:
		position = world.map_to_world(world.world_to_map(position))
	is_smooth_movement = !is_smooth_movement

func interact():
	if $Facing.is_colliding():
		var obj = $Facing.get_collider()
		if obj.has_method('interacted_with'):
			obj.interacted_with(self)

func move(v: Vector2):
	$StateMachine.move(v)

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.is_action('ui_cycle_mov'):
				if $StateMachine.state == $StateMachine/Sits:
					$StateMachine.transition_to($StateMachine/GridMove)
				elif $StateMachine.state == $StateMachine/GridMove:
					$StateMachine.transition_to($StateMachine/SmoothMove)
				elif $StateMachine.state == $StateMachine/SmoothMove:
					$StateMachine.transition_to($StateMachine/JumpGrid)
				elif $StateMachine.state == $StateMachine/JumpGrid:
					$StateMachine.transition_to(get_node('StateMachine/♞Movement'))
				elif $StateMachine.state == get_node('StateMachine/♞Movement'):
					$StateMachine.transition_to($StateMachine/GridMove)
				else:
					print('Error switching states')

## This function is fairly self-exlplanitory. When called it will flip the sprite up/down/left/right
func face(dir: Vector2):
	facing.cast_to = dir.normalized() * world.cell_size
	if abs(dir.x) < abs(dir.y):
		$AnimationPlayer.play('Down' if dir.y > 0 else 'Up')
	else:
		$AnimationPlayer.play('Right' if dir.x > 0 else 'Left')

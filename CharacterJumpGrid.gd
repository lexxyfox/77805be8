extends CharacterGridMovement

func move(dir: Vector2):
	character.face(dir)
	if character.get_node('Tween').is_active():
		return
	character.ray.cast_to = dir * character.world.cell_size * 2
	character.ray.force_raycast_update()
	if character.ray.is_colliding():
		character.ray.cast_to = dir * character.world.cell_size * 1
		character.ray.force_raycast_update()
		if character.ray.is_colliding():
			return # bump
	var target = character.position + character.ray.cast_to
	var duration = (character.position.distance_to(target) / character.world.cell_size.x) / character.speed
	character.get_node('Tween').interpolate_property(
		character, 'position', character.position, target,
		duration,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	character.get_node('Tween').interpolate_property(
		character.get_node('Sprite'), 'offset', Vector2(0, -10), Vector2(0, 0),
		duration,
		Tween.TRANS_CIRC,
		Tween.EASE_IN
	)
	character.get_node('Tween').stop(character, 'position')
	character.get_node('Tween').start()
	yield(character.get_node('Tween'), "tween_completed")

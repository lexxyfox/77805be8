GODOT_VER := 3.4.2
GODOT := 3rdparty/Godot_v$(GODOT_VER)-stable_mono_linux_headless_64/Godot_v$(GODOT_VER)-stable_mono_linux_headless.64
JSCC_VER := 20200719
JSCC_JAR := 3rdparty/closure-compiler-v$(JSCC_VER).jar
JS := java -jar '$(JSCC_JAR)' --language_in=ECMASCRIPT_2019 --language_out=ECMASCRIPT_2019 --compilation_level=ADVANCED --jscomp_off='*'
CP := cp --reflink=auto --sparse=always
PNG := pngcrush -v -blacken -brute -reduce -rem alla 2> /dev/null
HTML := foxxo/html_min
CSS := cleancss -O2 'restructureRules:on;mergeSemantically:on;mergeIntoShorthands:on'

.PHONY: cucumbered uncucumbered web-ready

cucumbers := \
	assets/cucumbered/DoorSilver.png \
	assets/cucumbered/light_256.png \
	assets/cucumbered/NotoColorEmoji.ttf \
	assets/cucumbered/pai.png \
	assets/cucumbered/skybox.png \
	assets/cucumbered/table0_48.png \
	assets/cucumbered/table1_48.png \
	assets/cucumbered/table10_48.png \
	assets/cucumbered/table11_48.png \
	assets/cucumbered/table2_48.png \
	assets/cucumbered/table3_48.png \
	assets/cucumbered/table4_48.png \
	assets/cucumbered/table5_48.png \
	assets/cucumbered/table6_48.png \
	assets/cucumbered/table7_48.png \
	assets/cucumbered/table8_48.png \
	assets/cucumbered/table9_48.png \
	assets/cucumbered/wall0_48.png \
	assets/cucumbered/wall1_48.png \
	assets/cucumbered/wall10_48.png \
	assets/cucumbered/wall11_48.png \
	assets/cucumbered/wall12_48.png \
	assets/cucumbered/wall13_48.png \
	assets/cucumbered/wall14_48.png \
	assets/cucumbered/wall15_48.png \
	assets/cucumbered/wall16_48.png \
	assets/cucumbered/wall2_48.png \
	assets/cucumbered/wall3_48.png \
	assets/cucumbered/wall4_48.png \
	assets/cucumbered/wall5_48.png \
	assets/cucumbered/wall6_48.png \
	assets/cucumbered/wall7_48.png \
	assets/cucumbered/wall8_48.png \
	assets/cucumbered/wall9_48.png \
	assets/cucumbered/window0_48.png \
	assets/cucumbered/window1_48.png \
	assets/cucumbered/window2_48.png \
	assets/cucumbered/window3_48.png \


cucumbered: $(cucumbers)

uncucumbered:
	$(RM) $(cucumbers)

assets/cucumbered/skybox.png: 3rdparty/cs13/icons/skybox/skybox.dmi
	foxxo/remap_atlas < '$<' > '$@' 736 3 \
		0 2 3 5 6 7 8 9 10 4 1

assets/cucumbered/pai.png: 3rdparty/cs13/icons/mob/pai_vr.dmi
	foxxo/docatlas 32 < '$<' > '$@' 2> /dev/null

assets/cucumbered/pai-fox.png: 3rdparty/cs13/icons/mob/pai_vr.dmi
	foxxo/remap_atlas < '$<' 32 4 \
		368 372 376 380 369 373 377 381 370 374 378 382 371 375 379 383 > '$@'

assets/cucumbered/NotoColorEmoji.ttf:
	curl -Lsf 'https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji.ttf' > '$@'

assets/cucumbered/DoorSilver.png: 3rdparty/cs13/icons/obj/doors/Doorsilver.dmi
	foxxo/docatlas 32 < '$<' > '$@' 2> /dev/null

assets/cucumbered/light_256.png: 3rdparty/cs13/icons/effects/light_overlays/light_256.dmi
	foxxo/png_compressify < '$<' > '$@' 2> /dev/null

assets/cucumbered/window0_48.png: 3rdparty/cs13/icons/obj/structures_vr.dmi
	foxxo/extract_window20 51 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/window1_48.png: 3rdparty/cs13/icons/obj/structures_vr.dmi
	foxxo/extract_window20 87 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/window2_48.png: 3rdparty/cs13/icons/obj/structures_vr.dmi
	foxxo/extract_wall20 153 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/window3_48.png: 3rdparty/cs13/icons/obj/structures_vr.dmi
	foxxo/extract_wall20 186 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table0_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 7 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table1_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 55 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table2_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 87 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table3_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 135 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table4_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 167 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table5_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 215 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table6_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 247 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table7_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 296 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table8_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 376 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table9_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	#todo: fix corners
	foxxo/extract_wall20 428 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table10_48.png: 3rdparty/cs13/icons/obj/tables_vr.dmi
	foxxo/extract_wall20 492 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/table11_48.png: 3rdparty/cs13/icons/obj/tables.dmi
	foxxo/extract_wall20 424 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall0_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 1 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall1_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 44 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall2_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 87 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall3_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 130 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall4_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 169 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall5_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 207 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall6_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 240 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall7_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 275 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall8_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 318 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall9_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 371 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall10_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 422 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall11_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 467 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall12_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 520 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall13_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 564 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall14_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 608 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall15_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 652 < '$<' | foxxo/blob20_to_blob48 > '$@'

assets/cucumbered/wall16_48.png: 3rdparty/cs13/icons/turf/wall_masks.dmi
	foxxo/extract_wall20 696 < '$<' | foxxo/blob20_to_blob48 > '$@'




$(GODOT):
	curl -Lsf 'https://github.com/godotengine/godot/releases/download/$(GODOT_VER)-stable/Godot_v$(GODOT_VER)-stable_mono_linux_headless_64.zip' \
		| bsdtar -xmf- -C 3rdparty
	chmod u+x '$@'

$(JSCC_JAR):
	curl -Lsf 'https://dl.google.com/closure-compiler/compiler-$(JSCC_VER).zip' \
		| bsdtar -xmf- -C 3rdparty closure-compiler-v$(JSCC_VER).jar

templates/webassembly_release.zip: \
	templates/webassembly/godot.audio.worklet.js \
	templates/webassembly/godot.html \
	templates/webassembly/godot.js \
	templates/webassembly/godot.offline.html \
	templates/webassembly/godot.service.worker.js \
	templates/webassembly/godot.wasm
	advzip -a4 '$@' \
		templates/webassembly/godot.audio.worklet.js \
		templates/webassembly/godot.html \
		templates/webassembly/godot.js \
		templates/webassembly/godot.offline.html \
		templates/webassembly/godot.service.worker.js \
		templates/webassembly/godot.wasm

build/index.144x144.png \
build/index.180x180.png \
build/index.512x512.png \
build/index.apple-touch-icon.png \
build/index.audio.worklet.js \
build/index.html \
build/index.icon.png \
build/index.js \
build/index.manifest.json \
build/index.offline.html \
build/index.pck \
build/index.png \
build/index.service.worker.js \
build/index.wasm \
&: $(GODOT) templates/webassembly_release.zip
	mkdir -p build
	$(GODOT) --verbose --export 'HTML5' build/index.html

build/combined.js: build/index.html build/index.js
	$(CP) build/index.js '$@'
	xmlstarlet fo -H -R build/index.html 2> /dev/null | \
		xmlstarlet sel -T -t -v '//script[not(@src)]' >> '$@'

public/index.css: build/index.html
	xmlstarlet fo -H -R build/index.html 2> /dev/null | \
		xmlstarlet sel -T -t -v '//style' | $(CSS) >> '$@'

public/index.144x144.png: build/index.144x144.png
	$(PNG) '$<' '$@'

public/index.180x180.png: build/index.180x180.png
	$(PNG) '$<' '$@'

public/index.512x512.png: build/index.512x512.png
	$(PNG) '$<' '$@'

public/index.apple-touch-icon.png: build/index.apple-touch-icon.png
	$(PNG) '$<' '$@'

public/index.icon.png: build/index.icon.png
	$(PNG) '$<' '$@'

public/index.png: build/index.png
	$(PNG) '$<' '$@'

public/index.audio.worklet.js: build/index.audio.worklet.js $(JSCC_JAR)
	$(JS) < '$<' > '$@'

public/index.service.worker.js: build/index.service.worker.js $(JSCC_JAR)
	$(JS) < '$<' > '$@'

public/index.js: build/combined.js $(JSCC_JAR)
	$(JS) < '$<' > '$@'

public/index.html: build/index.html
	xmlstarlet fo -N -H -R '$<' 2>/dev/null \
		| xmlstarlet ed -O -d '//script[not(@src)]' -d '//style' \
			-s '//head' -t elem -n link -v '' --var link '$$prev' \
			-i '$$link' -t attr -n rel -v stylesheet \
			-i '$$link' -t attr -n href -v index.css \
		| $(HTML) > '$@'

public/index.offline.html: build/index.offline.html
	$(HTML) < '$<' > '$@'

public/index.manifest.json: build/index.manifest.json
	$(CP) '$<' '$@'

public/index.pck: build/index.pck
	$(CP) '$<' '$@'

public/index.wasm: build/index.wasm
	$(CP) '$<' '$@'

web-ready: \
	public/index.144x144.png \
	public/index.180x180.png \
	public/index.512x512.png \
	public/index.apple-touch-icon.png \
	public/index.audio.worklet.js \
	public/index.css \
	public/index.html \
	public/index.icon.png \
	public/index.js \
	public/index.manifest.json \
	public/index.offline.html \
	public/index.pck \
	public/index.png \
	public/index.service.worker.js \
	public/index.wasm \

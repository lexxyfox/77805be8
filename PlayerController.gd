extends Node

# https://www.reddit.com/r/godot/comments/aw7qoj/comment/ehl4tiy

var target: Node = null

func _enter_tree():
	target = get_parent()

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.is_action('ui_accept'):
				target.interact()
			if event.is_action('ui_sit'):
				target.toggle_sit()

func _process(_delta):
	var input_direction: Vector2 = get_input_direction()
	if not input_direction:
		return
	target.move(input_direction)

## This function will return the input direction. `ui_<direction>` is determined by the project
## settings under input map.
func get_input_direction():
	return Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	)

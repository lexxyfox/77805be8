extends CharacterState

var prev_state : CharacterState = self

func enter(msg = null):
	if msg == null || !msg.has('PrevState'):
		prev_state = $"../GridMove"
	else:
		prev_state = msg["PrevState"]
	if character.facing.cast_to.x < 0:
		character.get_node('AnimationPlayer').play('SitLeft')
	else:
		character.get_node('AnimationPlayer').play('SitRight')

func move(dir: Vector2):
	return

extends TileMap

func _ready():
	var player = load("res://Character.tscn").instance()
	var controller = preload("res://PlayerController.gd").new()
	player.add_child(controller)
	self.add_child(player)
	player.position = map_to_world(Vector2(0, -1))
